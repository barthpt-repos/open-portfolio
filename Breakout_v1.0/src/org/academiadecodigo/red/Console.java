package org.academiadecodigo.red;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Console {
    private static Menu myMenu = new Menu();
    public static void main(String[] args) throws InterruptedException {
        while (true){
            // Create empty background for correct size window
            // If change gamePlay, this need to correspond
            Rectangle blankField = new Rectangle(10,10,750,650);
            blankField.setColor(Color.WHITE);
            blankField.draw();

            // User choose Level on menu
            int chosenLevel = myMenu.menu();

            // Pressing Q for quiting game
            // Will go to background of last game played
            if (chosenLevel == -1){
                // Break Loop and activate System.exit();
                break;
            }
            // Garbage collector on initial background
            blankField.delete();
            // Start Game with level chosen
            gameOn(chosenLevel);
        }

        System.exit(0);
    }

    private static void gameOn(int chosenLevel) throws InterruptedException {
        // Game On will instantiate new game with field and delay
        // Words good with x=750, y=650, delay=2
        // delay can be 1. But need to change speedFactor constant
        // delay=2 => speedFactor = 2 * chosenLevel
        // delay=1 => speedFactor = 1,25 to 1,5 * chosenLevel
        GamePlay firstTry = new GamePlay(750,650, 2, chosenLevel);

        // Init all and play game
        firstTry.init();
        firstTry.play();
        // After gamer over or win game. Garbage collect everything
        System.gc();

    }
}
