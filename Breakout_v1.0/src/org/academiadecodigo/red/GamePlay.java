package org.academiadecodigo.red;

import org.academiadecodigo.red.Mechanics.Ball;
import org.academiadecodigo.red.Mechanics.GameField;
import org.academiadecodigo.red.Mechanics.MapPosition;
import org.academiadecodigo.red.Mechanics.UserPaddle;
import org.academiadecodigo.red.bricks.BrickBuilder;
import org.academiadecodigo.red.bricks.MappedBricks;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Ellipse;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class GamePlay implements KeyboardHandler {
    private GameField field;
    private Ball ball;
    private UserPaddle player;
    private int delay;
    private BrickBuilder brickHandler;
    private Text score;
    private boolean gameOn;
    private int life;
    private final LinkedList<Ellipse> ballLife =new LinkedList();
    private LinkedHashMap<String,Integer> gameInfo = new LinkedHashMap<>();

    public GamePlay(int width, int height, int delay, int levelChosen){
        // Create key instances with custom level
        field = new GameField(width, height, levelChosen);
        ball = new Ball(levelChosen);
        player = new UserPaddle(field,levelChosen);

        // Store game info. Need improvement
        this.delay = delay;
        gameOn = false;
        gameInfo.put("Score", 0);
        gameInfo.put("col",10);
        gameInfo.put("row",10);
        life = 3;
        gameInfo.put("Life",life);
        gameInfo.put("LevelChosen",levelChosen);
        gameInfo.put("Delay",delay);
        gameInfo.put("timePlayed[s]",0);

        // Create Hash Map with all brick
        brickHandler = new BrickBuilder(gameInfo.get("col"),gameInfo.get("row"), levelChosen);

        // Graphics for how much life has left
        for (int i = 0;i<life;i++){
            ballLife.add(new Ellipse(width-20-30*i,20,20,20));
        }
        score = new Text(20,20,"Score: "+gameInfo.get("Score")+ " Time: " + gameInfo.get("timePlayed[s]") + "s");
    }

    public void init(){
        // Initialise all objects. Mainly draw/fill then
        field.init();
        ball.init();
        player.init();
        brickHandler.init();
        keyboardInit();
        for (Ellipse life:ballLife){
            life.setColor(Color.WHITE);
            life.fill();
        }

        score.draw();
    }
    public void play()throws InterruptedException {
        // Running game on one Thread

        // Initialise local variables
        boolean matchOver = false;
        int cycle = 0;
        int padPersistent =0;
        Picture gameOver = null;

        // Initialise game mechanics
        while (!matchOver) {

            // 1 or 2 milliseconds delay
            // height CPU demand...
            Thread.sleep(delay);

            // Pause button
            if (gameOn) {
                // Vector movement. Ball will get in objects first
                ball.move();

            }
            // ball avoid one cycle if hit pad


            // Check ball height for ball interaction
            if (cycle >= padPersistent &&
                    ball.getLowerPos() < (field.getFieldPos().lowerPos() -48.5)) {

                if (ball.intersection(player.getPaddlePos(), true)){
                    padPersistent = cycle + 4;
                }
            }

            // Check bricks collision
            for (String key : brickHandler.getBricHolder().keySet()) {
                // Only check collision for live bricks
                if (!brickHandler.getBricHolder().get(key).isDead()) {

                    if (ball.intersection(brickHandler.getBricHolder().get(key).getBricksMap(), false)) {
                        brickHandler.getBricHolder().get(key).getHits();
                        gameInfo.replace("Score", gameInfo.get("Score")+10);
                        gameInfo.put("timePlayed[s]",(gameInfo.get("Delay") * gameInfo.get("Cycle"))/1000);
                        score.setText("Score: "+gameInfo.get("Score") + " Time: " + gameInfo.get("timePlayed[s]") + "s");
                        break;
                    }
                }
            }
            // Last collision checker. Field limits
            ball.atGridLimit(field);

            // add Cycle time. Cycles x Delay = time in milliseconds played
            cycle++;
            gameInfo.put("Cycle", cycle);
            // Update screen every second
            if (cycle % 200 ==0){
                // save Time in seconds
                gameInfo.put("timePlayed[s]",(gameInfo.get("Delay") * gameInfo.get("Cycle"))/1000);
                score.setText("Score: "+gameInfo.get("Score") + " Time: " + gameInfo.get("timePlayed[s]") + "s");
            }

            // Check conditions for removing 1 life
            if (ball.getLowerPos() > field.getFieldPos().lowerPos() - 20) {

                // Losing life sound
                makeFallenSound();

                // Reset ball and not everything
                ball.resetBall();

                life--;
                gameInfo.put("Life", life);
                // Removing and deleting visual life counter
                Ellipse image = ballLife.pollLast();
                if (image != null) {
                    image.delete();
                }
                //System.out.println("Player remaining life: " + life);

                // Pause game
                gameOn = false;

                // Life conditional for game over
                if (life <= 0) {
                    gameOver = new Picture(125, 150, "resources/gameOver.png");
                    gameOver.draw();
                    Thread.sleep(3000);
                    break;
                }
            }
            // Brick conditional for win game
            if (brickHandler.liveBricks() <= 0){
                matchOver = true;
                gameOver = new Picture(125, 150, "resources/congrats.png");
                gameOver.draw();
                winnerSound();
                Thread.sleep(3000);
            }
            //System.out.println((int)ball.getLowerPos() +" "+(field.getFieldPos().lowerPos()-35));
        }
        // Save gameInfo
        saveScore();
        // Remove everything for new game
        delete();
        gameOver.delete();
    }

    public void delete(){
        player.deletePad();
        ball.getBalLPic().delete();
        score.delete();
        field.delete();


        for (String key: brickHandler.getBricHolder().keySet()) {
            if(!brickHandler.getBricHolder().get(key).isDead()){
                brickHandler.getBricHolder().get(key).getBrickPic().delete();
            }
        }
    }
    private void keyboardInit(){
        Keyboard keyboard = new Keyboard(this);

        //SPACE KEY to game paused
        KeyboardEvent spacePressed = new KeyboardEvent();
        spacePressed.setKey(KeyboardEvent.KEY_SPACE);
        spacePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(spacePressed);

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if(keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE &&
                    gameInfo.get("Life") > 0 ) {

            gameOn = !gameOn;
            player.setPause(gameOn);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
    private void saveScore() {
        try {
            BufferedWriter bsavedGame = new BufferedWriter(new FileWriter("lastPlay.txt"));
            bsavedGame.write(String.valueOf(gameInfo)+ "\n");

            bsavedGame.flush();
            bsavedGame.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadScore(){

    }

    private void makeFallenSound() {
        //File fallenSound = new File("resources/gosound.wav");
        try {
            Clip fallSound = AudioSystem.getClip();
            fallSound.open(AudioSystem.getAudioInputStream(getClass().getResource("/resources/gosound.wav")));
            fallSound.start();
        } catch (Exception e) {
            System.out.println("Error playing fallen sound");
        }
    }
    private void winnerSound() {
        //File winSound = new File("resources/winSound.wav");
        try {
            Clip win = AudioSystem.getClip();
            win.open(AudioSystem.getAudioInputStream(getClass().getResource("/resources/winSound.wav")));
            win.start();
        } catch (Exception e) {
            System.out.println("Error playing fallen sound");
        }
    }

}

