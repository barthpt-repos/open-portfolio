package org.academiadecodigo.red.Mechanics;

import org.academiadecodigo.simplegraphics.pictures.Picture;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class Ball {
    private Picture ball;
    private MapPosition ballPos;
    private double[] vector;

    // tested 1 to 6. Higher it is, higher crash problems
    private double speedFactor;
    private String filePath;

    //Need to be max |0.2|
    private double[] randomAngle;

    public Ball(int chosenLevel){
        if (chosenLevel == 1){
            filePath = "resources/Footballball.png";
        }
        if (chosenLevel == 2){
            filePath = "resources/ball.png";
        }
        if (chosenLevel == 3){
            filePath = "resources/spaceBall.png";
        }
        double cl = chosenLevel;
        if (cl != 1){
            cl = (chosenLevel == 2) ? 1.5 : 1.8;
        } else{
            cl = 1.2;
        }
        this.speedFactor = 1.5 * cl;
        randomAngle = new double[5];
        drawBall();
    }
    private void drawBall(){
        ballPos = new MapPosition(12,580,20,20);
        ball = new Picture(ballPos.drawPos()[0],ballPos.drawPos()[1], filePath);

        // Random choose 20% of angle variance
        double startAngle = Math.random()*0.11 * speedFactor;
        vector = new double[]{ 0.2 * speedFactor - startAngle,
                                -0.41 * speedFactor + startAngle};
    }
    public void init(){
        randomAngle[4] = (Math.abs(vector[0]) + Math.abs(vector[1]));
        ball.draw();

    }
    public void move(){
        // Update position, for using as boundary conditional
        // By summing vector with current position
        ballPos.setXY(ballPos.XY()[0] += vector[0], ballPos.XY()[1] += vector[1]);

        // Move ball with vector
        ball.translate(vector[0], vector[1]);
    }

    public boolean intersection(MapPosition pos, boolean isPlayer){
        boolean withIn = false;
            if ((ballPos.rightPos()) >= pos.leftPos() &&
                    ballPos.leftPos() <= pos.rightPos() &&
                        ballPos.lowerPos() >= pos.upperPos() &&
                            ballPos.upperPos() <= pos.lowerPos()) {
                double momentum = (ballPos.centerX() - pos.centerX()) / (pos.width() / 2);
                momentum = Math.abs(momentum);
                /*
                System.out.println("Ball with " + ballPos);
                System.out.println("Pos  with " + pos);
                System.out.println("Speed " + Arrays.toString(vector));
                System.out.println("Momentum " + momentum);
                System.out.println("New vector " + Arrays.toString(vector));
                */
                withIn = true;
                if (ballPos.leftPos()+2 >= pos.rightPos()) {
                    vector[0] = Math.abs(vector[0]);
                } else if ((ballPos.rightPos()-2 <= pos.leftPos())) {
                    vector[0] = -Math.abs(vector[0]);
                    // Only Player can change ball angle
                } else if (isPlayer) {

                    //Using momemtum for centralise ball and block lower angles than 60 degrees
                    if (Math.abs(vector[1] / vector[0]) <= 0.6 ||
                            // If x is too small (too straight) change angle to downward
                            momentum < 0.4 && Math.abs(vector[0]/vector[1]) > 0.2 ) {
                        randomAngle();

                        // Increase Y direction. Changing angle upper-ward
                        vector[1] =  -( (vector[1] > 0) ? (vector[1] + randomAngle[1]) :
                                                        (vector[1] - randomAngle[1]));
                        vector[0] =  ( (vector[0] > 0) ? (vector[0] - randomAngle[0]) :
                                                        (vector[0] + randomAngle[0]));

                    } else {
                        // Increase X direction. Changing angle downward
                        vector[1] =  -( (vector[1] > 0) ? (vector[1] - randomAngle[1]) :
                                                        (vector[1] + randomAngle[1]));
                        vector[0] =  ( (vector[0] > 0) ? (vector[0] + randomAngle[0]) :
                                                        (vector[0] - randomAngle[0]));
                    }

                } else {
                    // Simple change direction
                    vector[1] = -vector[1];
                }
                makePoingSound(); //change by line 77
            }
            return withIn;
        }

    private void randomAngle() {
        randomAngle[3] = (Math.abs(vector[0]) + Math.abs(vector[1]));
        //Check if speed is 20% above initial value
        if (randomAngle[3] > randomAngle[4]*1.2){
            // Decrement complementary, to reset ball speed
            randomAngle[3] = randomAngle[4]*0.85;
        }
        // Calculate random 20% of change. To remove from x/y and increment y/x
        randomAngle[2] = Math.random() * 0.2 * randomAngle[3];
        randomAngle[0] = randomAngle[2];
        randomAngle[1] = randomAngle[2];
    }

    public void atGridLimit(GameField field){
        if (ballPos.rightPos() >= field.getFieldPos().rightPos() ||
                ballPos.leftPos() <= field.getFieldPos().leftPos() ) {
            bounce(0);
        }

        if (ballPos.lowerPos() >= field.getFieldPos().lowerPos() ||
                ballPos.upperPos() <= field.getFieldPos().upperPos() ) {
            bounce(1);
        }
    }

    private void bounce(int changeVector){
        // Bounce is changing only one direction at two-dimensional array
        // If axis is out, change by - axis vector
        for (int i=0;i<2;i++){
            if (changeVector == i){
                vector[i] = -vector[i];
            }
        }
    }

    public double getLowerPos(){
        return ballPos.lowerPos();
    }

    public Picture getBalLPic(){
        return ball;
    }

    public void resetBall() {
        ball.delete();
        drawBall();
        ball.draw();
    }

    private void makePoingSound () {
        //File poingSound = new File("resources/poing.wav");
        //clip.open(AudioSystem.getAudioInputStream(myStreamPoing));
        //clip.start();

        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream poing = AudioSystem.getAudioInputStream(getClass().getResource("/resources/poing.wav"));
            clip.open(poing);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
