package org.academiadecodigo.red.Mechanics;

import org.academiadecodigo.red.Mechanics.MapPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameField {
    private Picture field;
    private String filePath;
    public MapPosition fieldPos;

    public GameField(int width, int height, int chosenLevel){
        if (chosenLevel == 1){
            filePath = "resources/Footballbackground.png";
        }
        if (chosenLevel == 2){
            filePath = "resources/beachBackground.jpg";
        }
        if (chosenLevel == 3){
            filePath = "resources/spaceBg.png";
        }

        fieldPos = new MapPosition(10, 10, width, height);
        //backField = new Rectangle(fieldPos.drawPos()[0],fieldPos.drawPos()[1],fieldPos.drawPos()[2],fieldPos.drawPos()[3]);
        field = new Picture(fieldPos.drawPos()[0], fieldPos.drawPos()[1],filePath);

    }
    public void init(){
        //backField.setColor(Color.MAGENTA);
        field.draw();
    }

    public MapPosition getFieldPos() {
        return fieldPos;
    }

    public void delete() {
        field.delete();
    }
}
