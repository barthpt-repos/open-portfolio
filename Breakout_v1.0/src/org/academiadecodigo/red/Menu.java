package org.academiadecodigo.red;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Menu implements KeyboardHandler {
    private int currentLevel;
    private Picture backgroundMenu;
    public int menu() throws InterruptedException{
        backgroundMenu = new Picture(10,10,"resources/menu.png");
        backgroundMenu.draw();
        keyboardInit ();
        currentLevel=0;
        while (currentLevel == 0){
            Thread.sleep(20);
        }
        backgroundMenu.delete();
        return currentLevel;
    }
    private void keyboardInit(){
        Keyboard keyboard = new Keyboard(this);
        //Press e for easy
        KeyboardEvent onePressed = new KeyboardEvent();
        onePressed.setKey(KeyboardEvent.KEY_1);
        onePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(onePressed);
        //Press m for easy
        KeyboardEvent twoPressed = new KeyboardEvent();
        twoPressed.setKey(KeyboardEvent.KEY_2);
        twoPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(twoPressed);
        //Press e for easy
        KeyboardEvent threePressed = new KeyboardEvent();
        threePressed.setKey(KeyboardEvent.KEY_3);
        threePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(threePressed);

        //Press e for easy
        KeyboardEvent qPressed = new KeyboardEvent();
        qPressed.setKey(KeyboardEvent.KEY_Q);
        qPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(qPressed);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if(keyboardEvent.getKey() == KeyboardEvent.KEY_1) {
            this.currentLevel=1;

        }
        if(keyboardEvent.getKey() == KeyboardEvent.KEY_2) {
            this.currentLevel=2;

        }
        if(keyboardEvent.getKey() == KeyboardEvent.KEY_3) {
            this.currentLevel=3;
        }

        if(keyboardEvent.getKey() == KeyboardEvent.KEY_Q) {
            this.currentLevel=-1;
        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
