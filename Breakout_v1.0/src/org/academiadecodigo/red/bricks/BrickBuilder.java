package org.academiadecodigo.red.bricks;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class BrickBuilder {
    private LinkedHashMap<String, MappedBricks> bricHolder = new LinkedHashMap();
    private final HashMap<String,Integer> bricksOption = new HashMap<>();
    private int[][] missingBricks;

    public BrickBuilder(int col, int row, int cl){
        // Create int[][] missingBricks for custom level brick pattern
        missingBricks = new int[6][4];
        if(cl != 1){
            cl = (cl ==2)? 3: 5;
        }
        // level 1 on col            <= == == >=
        missingBricks[0] = new int[]{ 0, 4, 5, 9};
        // level 1 on row
        missingBricks[1] = new int[]{ 3, 2, 7, 5};
        // level 2 on col
        missingBricks[2] = new int[]{ 0, 0, 9, 9};
        // level 2 on row
        missingBricks[3] = new int[]{ 0, 1, 5, 9};
        // level 3 on col
        missingBricks[4] = new int[]{ 0, 0, 9, 9};
        // level 3 on row
        missingBricks[5] = new int[]{ -1, -1, 10, 10};

        //Store game condition
        bricksOption.put("col",col);
        bricksOption.put("row", row);
        bricksOption.put("TotalBrick",col*row);

        // Create brick pattern.
        // Need implementation with missingBricks
        for (int i = 0; i < col; i++) {

            if ( i <= missingBricks[cl-1][0] || i == missingBricks[cl-1][1] ||
                    i == missingBricks[cl-1][2] || i >= missingBricks[cl-1][3] ) {
                continue;
            }
            for (int j = 0; j < row; j++) {

                if (j <= missingBricks[cl][0] || j == missingBricks[cl][1] ||
                        j == missingBricks[cl][2] || j >= missingBricks[cl][3] ) {
                    continue;
                }
                add( i, j, new MappedBricks(i,j));
            }
        }
    }
    public void init(){
        for (String key:bricHolder.keySet()){
            bricHolder.get(key).init();
        }
    }
    public void add(int col, int row, MappedBricks brick){
        bricHolder.put(col + " " + row, brick);
    }

    public int liveBricks(){
        int live = 0;
        for (String key: bricHolder.keySet()){
            if (!bricHolder.get(key).isDead()){
                live++;
            }
        }
        bricksOption.put("liveBricks",live);
        return live;
    }
    public LinkedHashMap<String, MappedBricks> getBricHolder(){
        return bricHolder;
    }

}
