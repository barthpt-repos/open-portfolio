package org.academiadecodigo.red.bricks;

import org.academiadecodigo.red.Mechanics.MapPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class MappedBricks {

    private Picture brickPics;
    private MapPosition bricksMap;
    private int hits;
    private boolean dead;
    public MappedBricks(int col, int row){
        int height = 25;
        int width = height * 3;
        bricksMap = new MapPosition(10 + (col * width),   10 + (row * height), width,height);
        brickPics = new Picture(bricksMap.drawPos()[0],bricksMap.drawPos()[1], "resources/Brick_Red.png");
        hits = 3;
        dead = false;
    }

    public void init(){
        brickPics.draw();

    }
    public int getHits() {
        hits--;
        if(hits ==2){
            brickPics.delete();
            brickPics = new Picture(bricksMap.drawPos()[0],bricksMap.drawPos()[1],
                    "resources/Brick_Orange_Broken.png");
            brickPics.draw();

        }
        if(hits ==1){
            brickPics.delete();
            brickPics = new Picture(bricksMap.drawPos()[0],bricksMap.drawPos()[1],
                    "resources/Brick_Yellow_All_Broken.png");
            brickPics.draw();
        }

        if( hits <= 0){
            dead = true;
            brickPics.delete();
            return 0;
        }
        return hits;
    }
    public boolean isDead() {
        return dead;
    }
    public MapPosition getBricksMap() {
        return bricksMap;
    }

    @Override
    public String toString(){
        return "Brick Position X: " + bricksMap.leftPos()+
                ", Position Y: " + bricksMap.upperPos() +
                " and Dead condition is " + dead +
                " with remaining hits " + hits;
    }
    public Picture getBrickPic(){
        return brickPics;
    }
}
