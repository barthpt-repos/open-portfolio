import psycopg2

user_password = input('Please inform Postgres password:')
# Connect to an existing database
conn = psycopg2.connect(user='postgres',
                        password=user_password,
                        database='postgres',
                        host='localhost',
                        port="5432")
user_password = ''

# Create a cursor to perform database operations
conn.autocommit = True
cur = conn.cursor()
cur.execute("SELECT 1 FROM pg_catalog.pg_database WHERE datname = 'group_9'")
check_db = cur.fetchone()
if not check_db:
    # '''ALTER DEFAULT privileges in schema public grant all on tables to u;'''
    cur.execute('DROP DATABASE IF EXISTS group_9')
    cur.execute('DROP ROLE IF EXISTS teacher')
    cur.execute("""CREATE USER teacher
                        WITH
                        ENCRYPTED PASSWORD 'FECD9'
                        SUPERUSER
                        """)
    cur.execute("""CREATE DATABASE group_9
                        WITH
                        OWNER teacher
                        ENCODING = 'UTF8'
                        LC_COLLATE = 'Portuguese_Brazil.1252'
                        LC_CTYPE = 'Portuguese_Brazil.1252'
                        """)
    print('\ndb group_9 created')
else:
    print('\ndb group_9 already created')

cur.close()
