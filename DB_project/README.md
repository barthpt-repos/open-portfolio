File content, organized by functional groups:

Folder Structured with the following files:
A1. fced-db-project-2021.pdf
A2. uml.png
A3. relational.txt

Python files:
B1. compare-countries.py
B2. load_countries_of_the_world.py
B3. load_happiness.py
B4. INSERT_ALL_WH.py
B5. CREATE DATABASE group_9

csv files:
countries_of_the_world.csv
C1. wh_2015.csv
C2. wh_2016.csv
C3. wh_2017.csv
C4. wh_2018.csv
C5. wh_2019.csv

"SQL Questions" folder with the following files:
D1. quesion.sql
D2. question_1.sql
D3. question_1 alt.sql
D4. question_2.sql
D5. question_3.sql
D6. question_4.sql
D7. question_4_part1.sql
D8. question_4_part2.sql
D9. question_4_part3.sql
D10. quetion_4_part4.sql
D11. question_5.sql

"SQL first run" folder with the following files:
E1. "1. CREATE_DATABASE.txt"
E2. CREATE_ROLE_teacher.sql
E3. CREATE_DATABASE_group_9.sql
E4. CREATE_tables.sql


For intro:
This folder

For system preparation, it is needed to install:
1. Local server of PostgresSQL
	We recommend PostgreSQL Version 14
	https://www.postgresql.org/download/

2. Python version 3.10:
	We recommend https://www.python.org/downloads/release/python-3100/

3. PostgreSQL and Python integration library psycopg:
	We recommend psycopg2 version 2.9.2
	https://www.psycopg.org/docs/install.html#install-from-source

4. Python's library Pandas for structured data manipulation:
	We recommend pandas version 1.3.4 thought PyPI
	type pip install pandas

Note: For more info on PyPI go to https://pypi.org/

Next, you can begin to setup the database in PostgreSQL.
Go to folder "SQL first run". 
There are two main ways to setup the database:
The first is by using .sql files on pgAdmin
The second is by executing a .py file using the command line. You will be asked to provide a postgres password!

With the database set up, it is time to load the data!

All files are already downloaded and are in this main folder.
To load them in the database there are two steps.

Go to the command line and type:
			load_countries_of_the_world.py

This will load the file "countries_of_the_world.csv" on the database.
For some misfortune, we prepped our script for CREATE IF NOT EXITS tables on that.
So if you forgot to load the third script, we got you covered!

After loading countries_of_the_world.csv, you can load World Happiness data as follows:

On the command line type load_happiness.py + file name.For example: 
			$ load_happiness.py wh_2018.csv

It is not needed to type any additional command.

However, do not forget to load all the data!

If you are not sure if you loaded all the data, type:

		load_happiness_all.py

in order to check.

And for final act:

On the folder "SQL Questions" you will find all the answers on question.sql

To load it,  we recommend using pgAdmin4 and executing "CREATE script on group_9 database".
But this script in not dynamic and has all the information at once.
So if you run it, be aware that it will just display the last query.

In order to make life easier, we divided all answers by query. 
So just load one at a time.
|
|
|
|
|
V
post scriptum!

For our preparation of the data and some analysis, we used the file compare-countries.py
If you run it on the command line, you will get some info about how we prepared our data!