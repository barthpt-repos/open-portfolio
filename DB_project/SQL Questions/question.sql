--Question 5a)
--What was the happiest country each year (year, country)?
SELECT name, score, year
FROM country JOIN metrics ON metrics.country_id = country.id
WHERE rank = 1
ORDER BY year DESC;

--Another way to answer 5a)
SELECT name, year
FROM metrics JOIN 
		country ON (metrics.country_id = country.id)
WHERE year = 2019 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2019 )
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2018 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2018 )
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2017 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2017)
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2016 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2016)
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2015 AND score = ( 
							SELECT MAX(score) 
							FROM metrics  
							WHERE year = 2015);
							
--Question 5b)
--What was the average happiness each year (year, happiness)?
SELECT year, AVG(score)
FROM metrics
GROUP BY year;

--Question 5c)
--In which position was Portugal in the happiness index each year (year, position)?
SELECT name, rank, year
FROM country JOIN metrics ON metrics.country_id = country.id
WHERE name = 'Portugal'
ORDER BY year DESC;

--Question 5d)
--What is the average happiness of the 10 countries with the higher GDP in each year (year, happiness)?
SELECT * 
FROM(
		(SELECT avg_score_15.year, avg(avg_score_15.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2015
			ORDER BY gdp DESC
			LIMIT 10
			) 
		as avg_score_15
		GROUP BY avg_score_15.year
	    ) 
	    UNION
	    (SELECT avg_score_16.year, avg(avg_score_16.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp is not NULL and year = 2016
			ORDER BY gdp DESC
			LIMIT 10
			) 
		as avg_score_16
		GROUP BY avg_score_16.year
	    )
	    UNION
	    (SELECT avg_score_17.year, avg(avg_score_17.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp is not NULL and year = 2017
			ORDER BY gdp DESC
			LIMIT 10
			) 
		as avg_score_17
		GROUP BY avg_score_17.year
	    )
	    UNION
	    (SELECT avg_score_18.year, avg(avg_score_18.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp is not NULL and year = 2018
			ORDER BY gdp DESC
			LIMIT 10
			) 
		as avg_score_18
		GROUP BY avg_score_18.year
		)
	    UNION
	    (SELECT avg_score_19.year, avg(avg_score_19.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp is not NULL and year = 2019
			ORDER BY gdp DESC
			LIMIT 10
			) 
		as avg_score_19
		GROUP BY avg_score_19.year
		)
	) 
	as mega_ugly_table
	GROUP BY mega_ugly_table.year, mega_ugly_table.avg;

--Question 5d)
--Avg score: 10 countries lower gdp?
SELECT * 
FROM(
		(SELECT avg_score_15.year, avg(avg_score_15.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2015
			ORDER BY gdp ASC
			LIMIT 10
			) 
		as avg_score_15
		GROUP BY avg_score_15.year
	    ) 
	    UNION
	    (SELECT avg_score_16.year, avg(avg_score_16.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2016
			ORDER BY gdp ASC
			LIMIT 10
			) 
		as avg_score_16
		GROUP BY avg_score_16.year
	    )
	    UNION
	    (SELECT avg_score_17.year, avg(avg_score_17.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2017
			ORDER BY gdp ASC
			LIMIT 10
			) 
		as avg_score_17
		GROUP BY avg_score_17.year
	    )
	    UNION
	    (SELECT avg_score_18.year, avg(avg_score_18.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2018
			ORDER BY gdp ASC
			LIMIT 10
			) 
		as avg_score_18
		GROUP BY avg_score_18.year
		)
	    UNION
	    (SELECT avg_score_19.year, avg(avg_score_19.score)
		FROM(
			SELECT name, gdp, year, score 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE gdp IS NOT NULL AND year = 2019
			ORDER BY gdp ASC
			LIMIT 10
			) 
		as avg_score_19
		GROUP BY avg_score_19.year
		)
	) 
	as mega_ugly_table
	GROUP BY mega_ugly_table.year, mega_ugly_table.avg;
	
	
-- Question 5d)
-- Avg score: 10 countries higher infant_mortality
SELECT * 
FROM(
		(SELECT avg_score_15.year, avg(avg_score_15.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2015
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_15
		GROUP BY avg_score_15.year
	    ) 
	    UNION
	    (SELECT avg_score_16.year, avg(avg_score_16.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality  IS NOT NULL AND year = 2016
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_16
		GROUP BY avg_score_16.year
	    )
	    UNION
	    (SELECT avg_score_17.year, avg(avg_score_17.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality  IS NOT NULL AND year = 2017
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_17
		GROUP BY avg_score_17.year
	    )
	    UNION
	    (SELECT avg_score_18.year, avg(avg_score_18.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2018
			ORDER BY infant_mortality  DESC
			LIMIT 10
			) 
		as avg_score_18
		GROUP BY avg_score_18.year
		)
	    UNION
	    (SELECT avg_score_19.year, avg(avg_score_19.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2019
			ORDER BY infant_mortality  DESC
			LIMIT 10
			) 
		as avg_score_19
		GROUP BY avg_score_19.year
		)
	) 
	as mega_ugly_table
	GROUP BY mega_ugly_table.year, mega_ugly_table.avg;
	
-- Question 5d)
-- Avg score: 10 countries higher literacy
SELECT * 
FROM(
		(SELECT avg_score_15.year, avg(avg_score_15.score)
		FROM(
			SELECT name, gdp, year, score, literacy
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE literacy IS NOT NULL AND year = 2015
			ORDER BY literacy DESC
			LIMIT 10
			) 
		as avg_score_15
		GROUP BY avg_score_15.year
	    ) 
	    UNION
	    (SELECT avg_score_16.year, avg(avg_score_16.score)
		FROM(
			SELECT name, gdp, year, score, literacy
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE literacy  IS NOT NULL AND year = 2016
			order by literacy DESC
			LIMIT 10
			) 
		as avg_score_16
		GROUP BY avg_score_16.year
	    )
	    UNION
	    (SELECT avg_score_17.year, avg(avg_score_17.score)
		FROM(
			SELECT name, gdp, year, score, literacy 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE literacy  IS NOT NULL AND year = 2017
			order by literacy DESC
			LIMIT 10
			) 
		as avg_score_17
		GROUP BY avg_score_17.year
	    )
	    UNION
	    (SELECT avg_score_18.year, avg(avg_score_18.score)
		FROM(
			SELECT name, gdp, year, score, literacy
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE literacy IS NOT NULL AND year = 2018
			order by literacy  DESC
			LIMIT 10
			) 
		as avg_score_18
		GROUP BY avg_score_18.year
		)
	    UNION
	    (SELECT avg_score_19.year, avg(avg_score_19.score)
		FROM(
			SELECT name, gdp, year, score, literacy 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE literacy IS NOT NULL AND year = 2019
			order by literacy  DESC
			LIMIT 10
			) 
		as avg_score_19
		GROUP BY avg_score_19.year
		)
	) 
	as mega_ugly_table
	GROUP BY mega_ugly_table.year, mega_ugly_table.avg;
	
-- Question e)
-- Defining improvement metric as improvement = score_2019 - score_2015, 
-- largest improvement case
SELECT name, scores_2019.score AS score_2019, scores_2015.score AS score_2015, (scores_2019.score - scores_2015.score) as improvement
FROM (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2019
		ORDER BY name) AS scores_2019
JOIN 
   (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2015
		ORDER BY name) AS scores_2015
using(name)
ORDER BY improvement desc
LIMIT 3;

-- largest regression case 
SELECT name, scores_2019.score AS score_2019, scores_2015.score AS score_2015, (scores_2019.score - scores_2015.score) AS improvement
FROM (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2019
		ORDER BY name) AS scores_2019
JOIN 
   (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2015
		ORDER BY name) AS scores_2015
using(name)
ORDER BY improvement ASC
LIMIT 3