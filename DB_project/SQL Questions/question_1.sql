SELECT name, year
FROM metrics JOIN 
		country ON (metrics.country_id = country.id)
WHERE year = 2019 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2019 )
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2018 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2018 )
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2017 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2017)
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2016 AND score = ( 
							SELECT MAX(score) 
							FROM metrics 
							WHERE year = 2016)
UNION ALL
SELECT name, year
FROM metrics JOIN country ON (metrics.country_id = country.id)
WHERE year = 2015 AND score = ( 
							SELECT MAX(score) 
							FROM metrics  
							WHERE year = 2015)