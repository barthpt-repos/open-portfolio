SELECT name, score, year
FROM country JOIN metrics ON metrics.country_id = country.id
WHERE rank = 1
ORDER BY year DESC