SELECT name, rank, year
FROM country JOIN metrics ON metrics.country_id = country.id
WHERE name = 'Portugal'
ORDER BY year DESC