-- d) avg score - 10 countries higher infant_mortality
SELECT * 
FROM(
		(SELECT avg_score_15.year, avg(avg_score_15.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality 
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2015
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_15
		GROUP BY avg_score_15.year
	    ) 
	    UNION
	    (SELECT avg_score_16.year, avg(avg_score_16.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality  IS NOT NULL AND year = 2016
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_16
		GROUP BY avg_score_16.year
	    )
	    UNION
	    (SELECT avg_score_17.year, avg(avg_score_17.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality  IS NOT NULL AND year = 2017
			ORDER BY infant_mortality DESC
			LIMIT 10
			) 
		as avg_score_17
		GROUP BY avg_score_17.year
	    )
	    UNION
	    (SELECT avg_score_18.year, avg(avg_score_18.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2018
			ORDER BY infant_mortality  DESC
			LIMIT 10
			) 
		as avg_score_18
		GROUP BY avg_score_18.year
		)
	    UNION
	    (SELECT avg_score_19.year, avg(avg_score_19.score)
		FROM(
			SELECT name, gdp, year, score, infant_mortality  
			FROM country JOIN metrics ON
			country.id = metrics.country_id
			WHERE infant_mortality IS NOT NULL AND year = 2019
			ORDER BY infant_mortality  DESC
			LIMIT 10
			) 
		as avg_score_19
		GROUP BY avg_score_19.year
		)
	) 
	as mega_ugly_table
	GROUP BY mega_ugly_table.year, mega_ugly_table.avg