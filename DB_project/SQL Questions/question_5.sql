-- e) Defining improvement metric as improvement = score_2019 - score_2015, 

-- largest improvement case
SELECT name, scores_2019.score AS score_2019, scores_2015.score AS score_2015, (scores_2019.score - scores_2015.score) as improvement
FROM (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2019
		ORDER BY name) AS scores_2019
JOIN 
   (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2015
		ORDER BY name) AS scores_2015
using(name)
ORDER BY improvement desc
LIMIT 3;

-- largest regression case 
SELECT name, scores_2019.score AS score_2019, scores_2015.score AS score_2015, (scores_2019.score - scores_2015.score) AS improvement
FROM (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2019
		ORDER BY name) AS scores_2019
JOIN 
   (SELECT name, score, year
		FROM country JOIN metrics 
		ON (country.id = metrics.country_id)
		WHERE year = 2015
		ORDER BY name) AS scores_2015
using(name)
ORDER BY improvement ASC
LIMIT 3;