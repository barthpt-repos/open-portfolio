-- Database: postgres

-- DROP DATABASE IF EXISTS postgres;
CREATE DATABASE group_9
    WITH 
    OWNER = teacher
    ENCODING = 'UTF8'
    LC_COLLATE = 'Portuguese_Brazil.1252'
    LC_CTYPE = 'Portuguese_Brazil.1252'
