GRANT ALL ON DATABASE group_9 TO teacher;

CREATE TABLE IF NOT EXISTS region (
	id INTEGER NOT NULL PRIMARY KEY, 
	name TEXT
    );
CREATE TABLE IF NOT EXISTS country (
	id INTEGER NOT NULL PRIMARY KEY, 
	name TEXT,
	population INTEGER,
	infant_mortality FLOAT,
	gdp FLOAT,
	literacy FLOAT,
	region_id INTEGER REFERENCES region
    );
CREATE TABLE IF NOT EXISTS metrics (
	id INTEGER PRIMARY KEY,
	score FLOAT,
	year INTEGER, 
	rank INTEGER,
	country_id INTEGER REFERENCES country,
	UNIQUE (year, country_id)
	);
commit