import pandas as pd
import warnings

warnings.filterwarnings('ignore')
# Importing file 'countries_of_the_world.csv' and unifying columns names
ls_ss = {'Area (sq. mi.)': 'area', 'Pop. Density (per sq. mi.)': 'density', 'Region':'region',
         'Literacy (%)':'literacy', 'Infant mortality (per 1000 births)':'infant_mortality',
         'GDP ($ per capita)':'gdp', 'Country':'country', 'Population':'population'}
cofw_df = pd.read_csv('countries_of_the_world.csv').rename(columns=ls_ss)

# Removing unneeded spaces and lowering case
cofw_df['country'] = cofw_df['country'].str.strip()

# Import file World Happiness by year and unifying columns
hr_s = {'Country or region':'country', 'Country':'country','Score':'score', 'Happiness.Rank':'rank',
        'Happiness Score':'score', 'Happiness.Score':'score', 'Happiness Rank':'rank', 'Overall rank':'rank'}

# Reading world happiness data and unifin columns name
wh_2015_df = pd.read_csv('wh_2015.csv').rename(columns=hr_s)
wh_2016_df = pd.read_csv('wh_2016.csv').rename(columns=hr_s)
wh_2017_df = pd.read_csv('wh_2017.csv').rename(columns=hr_s)
wh_2018_df = pd.read_csv('wh_2018.csv').rename(columns=hr_s)
wh_2019_df = pd.read_csv('wh_2019.csv').rename(columns=hr_s)

# Remove excess spaces
wh_2015_df['country'] = wh_2015_df['country'].str.strip()
wh_2016_df['country'] = wh_2016_df['country'].str.strip()
wh_2017_df['country'] = wh_2017_df['country'].str.strip()
wh_2018_df['country'] = wh_2018_df['country'].str.strip()
wh_2019_df['country'] = wh_2019_df['country'].str.strip()

def merging_in_out():
    # Merging all outter
    all_df = cofw_df.merge(wh_2019_df, on='country', how='outer'
                           ).merge(wh_2018_df, on='country', how='outer'
                                   ).merge(wh_2017_df, on='country', how='outer'
                                           ).merge(wh_2016_df, on='country', how='outer'
                                                   ).merge(wh_2015_df, on='country', how='outer')

    # Merging all inner
    inner_df = cofw_df.merge(wh_2019_df, on='country', how='inner'
                             ).merge(wh_2018_df, on='country', how='inner'
                                     ).merge(wh_2017_df, on='country', how='inner'
                                             ).merge(wh_2016_df, on='country', how='inner'
                                                     ).merge(wh_2015_df, on='country', how='inner')
    return all_df, inner_df


c, p = 0, 1
while c != p:
    all_df, inner_df = merging_in_out()
    c = (len(all_df['country']) - len(inner_df['country']))
    # Masking all that do not merge
    mask = ~all_df['country'].isin(inner_df['country'])

    # Making a series with all country that didn't fully merge
    S_all = all_df['country'][mask].sort_values()

    # Making a dict for all unique cases
    unit = {'Central African Rep.': 'Central African Republic', 'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
            'Congo, Repub. of the': 'Republic of the Congo', 'Congo (Brazzaville)': 'Republic of the Congo',
            'Somaliland region': 'Somaliland Region', 'Korea, South': 'South Korea', 'Korea, North': 'North Korea',
            'Congo (Kinshasa)': 'Kinshasa', 'Bosnia & Herzegovina': 'Bosnia and Herzegovina',
            "Cote d'Ivoire": 'Ivory Coast', 'Taiwan Province of China':'Taiwan', 'Hong Kong S.A.R., China':'Hong Kong',
            'British Virgin Is.': 'British Virgin Islands', 'Trinidad & Tobago': 'Trinidad and Tobago',
            'Northern Cyprus': 'North Cyprus',
            'Bahamas, The': 'Bahamas'}

    # Using dict to replace matches
    wh_2019_df['country'].replace(unit, inplace=True)
    wh_2018_df['country'].replace(unit, inplace=True)
    wh_2017_df['country'].replace(unit, inplace=True)
    wh_2016_df['country'].replace(unit, inplace=True)
    wh_2015_df['country'].replace(unit, inplace=True)
    cofw_df['country'].replace(unit, inplace=True)

    all_df_pos, inner_df_pos = merging_in_out()
    # counting Countries that are still left out
    p = (len(all_df_pos['country']) - len(inner_df_pos['country']))
print(f"There are {p} that are missing, at least, one or more files.")

# Make a dict to unify country names
unit = {'Central African Rep.': 'Central African Republic', 'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
        'Congo, Repub. of the': 'Republic of the Congo', 'Congo (Brazzaville)': 'Republic of the Congo',
        'Somaliland region': 'Somaliland Region', 'Korea, South': 'South Korea', 'Korea, North': 'North Korea',
        'Congo (Kinshasa)': 'Kinshasa', 'Bosnia & Herzegovina': 'Bosnia and Herzegovina',
        "Cote d'Ivoire": 'Ivory Coast', 'Taiwan Province of China': 'Taiwan', 'Hong Kong S.A.R., China': 'Hong Kong',
        'British Virgin Is.': 'British Virgin Islands', 'Trinidad & Tobago': 'Trinidad and Tobago',
        'Northern Cyprus': 'North Cyprus',
        'Bahamas, The': 'Bahamas'}

# Using dict to replace matches
wh_2019_df['country'].replace(unit, inplace=True)
wh_2018_df['country'].replace(unit, inplace=True)
wh_2017_df['country'].replace(unit, inplace=True)
wh_2016_df['country'].replace(unit, inplace=True)
wh_2015_df['country'].replace(unit, inplace=True)
cofw_df['country'].replace(unit, inplace=True)

# Run a merge (outer and inner) to get which country that, at least, are not in a single or more files
all_df_pos, inner_df_pos = merging_in_out()
check = all_df_pos[~all_df_pos['country'].isin(inner_df_pos['country'])]['country']

# Check how many country that which file has missing
match_wh15, match_wh16, match_wh17, match_wh18, match_wh19, match_cofw = [],[],[],[],[],[]
empty_wh15 = len(check) - wh_2015_df[wh_2015_df['country'].isin(check)]['country'].count()
empty_wh16 = len(check) - wh_2016_df[wh_2016_df['country'].isin(check)]['country'].count()
empty_wh17 = len(check) - wh_2017_df[wh_2017_df['country'].isin(check)]['country'].count()
empty_wh18 = len(check) - wh_2018_df[wh_2018_df['country'].isin(check)]['country'].count()
empty_wh19 = len(check) - wh_2019_df[wh_2019_df['country'].isin(check)]['country'].count()
check_in_cofw = cofw_df[cofw_df['country'].isin(check)]['country']
leftalt_cofw = check[~check.isin(check_in_cofw)]
empty_cofw = len(check) - check_in_cofw.count()

# Printing the results
print('And bellow is a missing number of countries by file name:')
print(f'WH15 will have {empty_wh15} empty values')
print(f'WH16 will have {empty_wh16} empty values')
print(f'WH17 will have {empty_wh17} empty values')
print(f'WH18 will have {empty_wh18} empty values')
print(f'WH19 will have {empty_wh19} empty values')
print(f'cofw will have {empty_cofw} empty values.')
#print('And the list of countries is ', leftalt_cofw.tolist())
