import psycopg2
import pandas as pd
import numpy as np
from textwrap import dedent
import warnings
warnings.filterwarnings('ignore')


# Importing file 'countries_of_the_world.csv' and unifying columns names
ls_ss = {'Area (sq. mi.)': 'area', 'Pop. Density (per sq. mi.)': 'density', 'Region':'region',
         'Literacy (%)':'literacy', 'Infant mortality (per 1000 births)':'infant_mortality',
         'GDP ($ per capita)':'gdp', 'Country':'country', 'Population':'population'}
cofw_df = pd.read_csv('countries_of_the_world.csv').rename(columns=ls_ss)

# Removing unneeded spaces, lowering case
cofw_df['region'] = cofw_df['region'].str.strip().str.lower()
cofw_df['country'] = cofw_df['country'].str.strip()

# Preparing numbers and filling np.nan with 0
cofw_df.fillna(0, inplace=True)
cofw_df['infant_mortality'] = cofw_df['infant_mortality'].str.replace(',','.').astype(float)
cofw_df['population'] = cofw_df['population'].astype(int)
cofw_df['gdp'] = cofw_df['gdp'].astype(float)
cofw_df['literacy'] = cofw_df['literacy'].str.replace(',','.').astype(float)

# Renaming some countries
unit = {'Central African Rep.': 'Central African Republic', 'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
        'Congo, Repub. of the': 'Republic of the Congo', 'Congo (Brazzaville)': 'Republic of the Congo',
        'Somaliland region': 'Somaliland Region', 'Korea, South': 'South Korea', 'Korea, North': 'North Korea',
        'Congo (Kinshasa)': 'Kinshasa', 'Bosnia & Herzegovina': 'Bosnia and Herzegovina',
        "Cote d'Ivoire": 'Ivory Coast', 'Taiwan Province of China': 'Taiwan', 'Hong Kong S.A.R., China': 'Hong Kong',
        'British Virgin Is.': 'British Virgin Islands', 'Trinidad & Tobago': 'Trinidad and Tobago',
        'Northern Cyprus': 'North Cyprus',
        'Bahamas, The': 'Bahamas'}

cofw_df['country'].replace(unit, inplace=True)

# Preparing dataframe for only needed columns
cofw_col_upload = ['country', 'region', 'population', 'infant_mortality', 'gdp', 'literacy']
prep_db_cofw = cofw_df[cofw_col_upload]

# And adding PK for region
n = 0
for reg in prep_db_cofw['region'].unique():
  n += 1
  prep_db_cofw.loc[prep_db_cofw['region'] == reg, 'region(id)'] = n

# Importing missing contries. Dataframe leftalt_cofw from preparation
missing_ct = ['Kosovo', 'North Cyprus', 'Montenegro', 'North Macedonia', 'Palestinian Territories',
              'Gambia', 'Kinshasa', 'Myanmar', 'South Sudan', 'Somaliland Region']

# Adding new row with country with no metrics or region
for ct in missing_ct:
    new_row = {cofw_col_upload[0]:ct, cofw_col_upload[1]:np.nan, cofw_col_upload[2]:np.nan,
               cofw_col_upload[3]:np.nan, cofw_col_upload[4]:np.nan, cofw_col_upload[5]:np.nan}
    prep_db_cofw = prep_db_cofw.append(new_row, ignore_index=True)

# Formating 0 and np.nan
prep_db_cofw[cofw_col_upload[2:]] = prep_db_cofw[cofw_col_upload[2:]].replace(0,  'NULL')
prep_db_cofw[cofw_col_upload[2:]] = prep_db_cofw[cofw_col_upload[2:]].fillna('NULL')

# Formatig region and adding new region: Missing Region
prep_db_cofw['region(id)'] = prep_db_cofw['region(id)'].fillna(12)
prep_db_cofw['region(id)'] = prep_db_cofw['region(id)'].astype(int)
missing_region = prep_db_cofw['region(id)']== 12
prep_db_cofw.loc[missing_region , 'region'] = 'Missing Region'


# Preparing string country and region for mass upload
country_up, region_up = '', ''
for i in prep_db_cofw.index:
    a,b,c = int(i+1), str(prep_db_cofw.iloc[i, 0]), prep_db_cofw.iloc[i, 2]
    d,e,f = prep_db_cofw.iloc[i, 3], prep_db_cofw.iloc[i, 4], prep_db_cofw.iloc[i, 5]
    g = prep_db_cofw.iloc[i, 6]
    #load_country(a,b,c,d,e,f)
    country_up += dedent('''INSERT INTO country(id, name, population, infant_mortality, gdp, literacy, region_id) 
    VALUES ( %s, '%s', %s, %s, %s, %s, %s);
    '''% (a,b,c,d,e,f,g))
    # Since regions in countries, using condition to parse only unique region
    if a <= prep_db_cofw['region(id)'].max():
      r = str(prep_db_cofw[prep_db_cofw['region(id)']== a]['region'].iloc[0])
      i = int(prep_db_cofw[prep_db_cofw['region(id)']== a]['region(id)'].iloc[0])
      region_up += dedent('''INSERT INTO region(id, name) VALUES ( %s, '%s');
      '''% (i,r))


# Connect to an existing database group_9
conn = psycopg2.connect(user='teacher',
                        password='FECD9',
                        database='group_9',
                        host='localhost',
                        port="5432")

# Create a cursor to perform database operations
conn.autocommit = True
cur = conn.cursor()
# CREATE or DELETE tables contents
cur.execute('''
    CREATE TABLE IF NOT EXISTS region (
        id INTEGER NOT NULL PRIMARY KEY, 
        name TEXT
    );   
    CREATE TABLE IF NOT EXISTS country (
        id INTEGER NOT NULL PRIMARY KEY, 
        name TEXT,
        population INTEGER,
        infant_mortality FLOAT,
        gdp FLOAT,
        literacy FLOAT,
        region_id INTEGER REFERENCES region
    );
    CREATE TABLE IF NOT EXISTS metrics (
        id INTEGER PRIMARY KEY,
        score FLOAT,
        year INTEGER, 
        rank INTEGER,
        country_id INTEGER REFERENCES country,
        UNIQUE (year, country_id)
    )''')

cur.execute('''
        DELETE FROM metrics;
        DELETE FROM country;
        DELETE FROM region
        ''')

# Execute mass INSERT
cur.execute('%s'% (region_up))
cur.execute('%s'% (country_up))
print('First, was created table (if not exist). \nAfter that, clear all data form all tables. \nThen loaded Contries of the World')

cur.close()
