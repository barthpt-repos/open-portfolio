import pandas as pd
import psycopg2
from textwrap import dedent
import warnings
import sys
warnings.filterwarnings('ignore')

# load string from user
load_file = str(sys.argv[1])

# Reading world happiness data from user argument and unifying columns name
hr_s = {'Country or region':'country', 'Country':'country', 'Score':'score', 'Happiness.Rank':'rank',
        'Happiness Score':'score', 'Happiness.Score':'score', 'Happiness Rank':'rank', 'Overall rank':'rank'}

try:
    wh_year_df = pd.read_csv(load_file).rename(columns=hr_s)
except:
    print(f'Please check file name: {load_file}\nFor doubt or guidance please goto README.txt on folder')
    quit()
# creating new column for year loaded
wh_year_df['year'] = int(load_file[3:7])

unit = {'Central African Rep.': 'Central African Republic', 'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
        'Congo, Repub. of the': 'Republic of the Congo', 'Congo (Brazzaville)': 'Republic of the Congo',
        'Somaliland region': 'Somaliland Region', 'Korea, South': 'South Korea', 'Korea, North': 'North Korea',
        'Congo (Kinshasa)': 'Kinshasa', 'Bosnia & Herzegovina': 'Bosnia and Herzegovina',
        "Cote d'Ivoire": 'Ivory Coast', 'Taiwan Province of China': 'Taiwan', 'Hong Kong S.A.R., China': 'Hong Kong',
        'British Virgin Is.': 'British Virgin Islands', 'Trinidad & Tobago': 'Trinidad and Tobago',
        'Northern Cyprus': 'North Cyprus',
        'Bahamas, The': 'Bahamas'}
wh_year_df['country'].replace(unit, inplace=True)

# Preparing dataframe for only needed columns
wh_col_upload = ['country','score', 'year', 'rank']
wh_year_df = wh_year_df[wh_col_upload]

# Connect to an existing database group_9
conn = psycopg2.connect(user='teacher',
                        password='FECD9',
                        database='group_9',
                        host='localhost',
                        port="5432")

# Create a cursor to perform database operations
conn.autocommit = True
cur = conn.cursor()

cur.execute('SELECT name, id FROM country')
# Not sure of the format
db_c_PK = cur.fetchall()
db_c_PK = pd.DataFrame(db_c_PK).rename(columns={0: 'country', 1: 'PK'})

# Replace country with retrived PK do database
wh_year_df['FK'] = wh_year_df['country'].map(db_c_PK.set_index('country')['PK'])

# DELETE TABLE metrics contents
cur.execute('DELETE FROM metrics WHERE year = %s' % int(load_file[3:7]))

# Collect PK from table metrics
cur.execute('SELECT MAX(id) FROM metrics')
# Check if Table is empty
# If not give last key used
m_PK = cur.fetchone()[0]
if m_PK is None:
    m_PK = 0

# Preparing string for mass upload
metrics_up = ''
for i in wh_year_df.index:
    a,b,c = int(i+m_PK+1), float(wh_year_df.iloc[i, 1]), int(wh_year_df.iloc[i, 2])
    d, e = wh_year_df.iloc[i, 3], wh_year_df.iloc[i, 4]
    metrics_up += dedent('''INSERT INTO metrics(id, score, year, rank, country_id) 
    VALUES ( %s, %s, %s, %s, %s);
    ''' % (a, b, c, d, e))

# Execute mass INSERT
cur.execute('%s' % metrics_up)
cur.close()
print(f'Successful upload data file {load_file} to DataBase group_9')