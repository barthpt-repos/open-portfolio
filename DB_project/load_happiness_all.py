import pandas as pd
import psycopg2
from textwrap import dedent
import warnings
import sys
warnings.filterwarnings('ignore')

# Rename Columns
hr_s = {'Country or region':'country', 'Country':'country', 'Score':'score', 'Happiness.Rank':'rank',
        'Happiness Score':'score', 'Happiness.Score':'score', 'Happiness Rank':'rank', 'Overall rank':'rank'}

# Reading world happiness data
wh_2015_df = pd.read_csv('wh_2015.csv').rename(columns=hr_s)
wh_2016_df = pd.read_csv('wh_2016.csv').rename(columns=hr_s)
wh_2017_df = pd.read_csv('wh_2017.csv').rename(columns=hr_s)
wh_2018_df = pd.read_csv('wh_2018.csv').rename(columns=hr_s)
wh_2019_df = pd.read_csv('wh_2019.csv').rename(columns=hr_s)

# Adding the year column
wh_2015_df["year"], wh_2016_df["year"], wh_2017_df["year"] = 2015, 2016, 2017
wh_2018_df["year"], wh_2019_df["year"] = 2018, 2019

# Preparing dataframe for only needed columns
wh_col_upload = ['country','score', 'year', 'rank']

# Merging at last row
prep_db_wh = wh_2015_df[wh_col_upload].merge(
    wh_2016_df[wh_col_upload], on=wh_col_upload, how='outer').merge(
    wh_2017_df[wh_col_upload], on=wh_col_upload, how='outer').merge(
    wh_2018_df[wh_col_upload], on=wh_col_upload, how='outer').merge(
    wh_2019_df[wh_col_upload], on=wh_col_upload, how='outer')

unit = {'Central African Rep.': 'Central African Republic', 'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
        'Congo, Repub. of the': 'Republic of the Congo', 'Congo (Brazzaville)': 'Republic of the Congo',
        'Somaliland region': 'Somaliland Region', 'Korea, South': 'South Korea', 'Korea, North': 'North Korea',
        'Congo (Kinshasa)': 'Kinshasa', 'Bosnia & Herzegovina': 'Bosnia and Herzegovina',
        "Cote d'Ivoire": 'Ivory Coast', 'Taiwan Province of China': 'Taiwan', 'Hong Kong S.A.R., China': 'Hong Kong',
        'British Virgin Is.': 'British Virgin Islands', 'Trinidad & Tobago': 'Trinidad and Tobago',
        'Northern Cyprus': 'North Cyprus',
        'Bahamas, The': 'Bahamas'}
prep_db_wh['country'].replace(unit, inplace=True)

# Connect to an existing database group_9
conn = psycopg2.connect(user='postgres',
                        password='KsLI5#gy}XW@yX?sl',
                        database='group_9',
                        host='localhost',
                        port="5432")

# Create a cursor to perform database operations
conn.autocommit = True
cur = conn.cursor()

cur.execute('SELECT name, id FROM country')
# Not sure of the format
db_c_PK = cur.fetchall()
db_c_PK = pd.DataFrame(db_c_PK).rename(columns={0: 'country', 1: 'PK'})

# Replace country with retrived PK do database
prep_db_wh['FK'] = prep_db_wh['country'].map(db_c_PK.set_index('country')['PK'])

# Preparing string for mass upload
metrics_up = ''
for i in prep_db_wh.index:
    a,b,c = int(i+1), float(prep_db_wh.iloc[i, 1]), int(prep_db_wh.iloc[i, 2])
    d, e = prep_db_wh.iloc[i, 3], prep_db_wh.iloc[i, 4]
    metrics_up += dedent('''INSERT INTO metrics(id, score, year, rank, country_id) 
    VALUES ( %s, %s, %s, %s, %s);
    ''' % (a, b, c, d, e))

# Connect to an existing database group_9
conn = psycopg2.connect(user='teacher',
                        password='FECD9',
                        database='group_9',
                        host='localhost',
                        port="5432")

# Create a cursor to perform database operations
conn.autocommit = True
cur = conn.cursor()
# CREATE or DELETE tables contents
cur.execute('DELETE FROM metrics')
print('Deleted all data from World Happiness')

# Execute mass INSERT
cur.execute('%s' % metrics_up)
cur.close()
print('Successful uploaded all files in this folder to Database')
