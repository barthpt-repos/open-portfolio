Welcome to my first open repository.
As all first time there will be a lot non standards...
But as I am learning to code for three months on java and one year in Python now, I am improving at lighting speed!

For start, 
I start to learn python for better perform and automate analysis for manufacturing industry.
So during my first semester of Data Science i have done three main projects.

[Statiscal Analysis](StatiscalAnalysis):
This project was done with two more colleagues and focus on research New York Police Stops and Frisk policy.
It was used 2020 year for the report.

[Industrial Signal Processing](SignalProcessing):
This project was done with three more colleagues and focus on clustering products processes by a induction heat treating industrial furnace. 
For the research, was chosen input power for analysis signal and pyrometer read from bar surface for output signal.
Since original data is lock by industrial data policy, report was done on verosimilarity data.

[Visual Study of Graphs](VisualAnalysis):
This project was done with one more colleague and focus on research Portugal imigrant data for the last 10 year. The goal was answer the following question:
Is the number of immigrants related to the number of crimes registered in Portugal and its main regions?
Data preparation was done in python, and graph analysis was done in R.

[Game: Kabum](Breakout_v1.0):
This game was doen with three more colleagues during game week of the Full Stack Bootcamp. All this code was done after hours for 10 nights and 9 days.
On top of that, we are only fourth week of learning java. So a lot of poor execution can be found on that code.
Current I'm working on version 1.1 to clean this up.
Next feature will be save and load ability.

[Game: Snake](Snake_v1.1):
This game was done solo by me as after classes project. The goal was to write cleaner code with some design pattern. 
Current working on write all game data to file.

[Hackathon: The Proclaimers](https://gitlab.com/barthpt-repos/hackathon):
This project was done as 24h hackathon to delivery a complete Web Application under a unknown topic. Since it was a competition, there were established criteria (described on requirement.md) and rules from compliance. 
This was group project were I implement solo the BackEnd on Spring Boot Framework to persist data on MySQL server. The other four integrants was on FrontEnd, as one on Design, one on Content delivery and two on integration and scripts. 
