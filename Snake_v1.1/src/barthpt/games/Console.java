package barthpt.games;

import barthpt.games.mechanic.Game;

public class Console {
    public static void main(String[] args) {
        Game snakeGame = new Game( 25, 600);
        snakeGame.play();
    }
}
