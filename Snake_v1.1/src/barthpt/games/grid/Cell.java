package barthpt.games.grid;

import barthpt.games.mechanic.DataMap;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell extends DataMap {
    private final Rectangle graphic;

    public Cell(int x, int y, double cellSizeX, double cellSizeY) {
        addXY(x,y);
        addPixel(x, y, cellSizeX, cellSizeY);

        dataBoolean.put("food", false);

        graphic = new Rectangle( getPos(0), getPos(1), cellSizeX, cellSizeY);
    }

    public void init() {
        graphic.setColor(Color.WHITE);
        graphic.draw();
    }
    private void addPixel(double x, double y, double cellSizeX, double cellSizeY) {
        dataDouble.put("X", 10 + x * cellSizeX);
        dataDouble.put("Width", 10 + (x + 1) * cellSizeX);
        dataDouble.put("Y", 10 + y * cellSizeY);
        dataDouble.put("Height", 10 + (y +1) * cellSizeY);
    }
    private void addXY(int x, int y) {
        dataInt.put("cellCol", x);
        dataInt.put("cellRow", y);
    }

    private double getPos(int index){
        if (index == 0) return dataDouble.get("X");
        if (index == 1) return dataDouble.get("Y");
        if (index == 2) return dataDouble.get("Width");
        if (index == 3) return dataDouble.get("Height");
        return -1;
    }
    public void snakeIn() {
        graphic.setColor(Color.DARK_GRAY);
        graphic.fill();
    }
    public void snakeOut() {
        graphic.setColor(Color.WHITE);
        graphic.draw();
    }
    public void foodOn() {
        dataBoolean.replace("food",true);
        graphic.setColor(Color.GREEN);
        graphic.fill();
    }
    public boolean isFood() {
        return dataBoolean.get("food");
    }
    public void eatFood() {
        dataBoolean.replace("food",false);
    }

    @Override
    public String toString(){
        return "{X="+ dataDouble.get("col")+ ", Y="+ dataDouble.get("row")+", food="+ dataDouble.get("food")+"}";
    }

    @Override
    public void saveData() {

    }
}
