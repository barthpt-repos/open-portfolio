package barthpt.games.grid;

public enum Direction {
    UP(-1),
    DOWN(1),
    RIGHT(100),
    LEFT(-100);

    private int dir;
    Direction(int direction) {
        dir = direction;
    }

    public int getDir() {
            return dir;
        }
}
