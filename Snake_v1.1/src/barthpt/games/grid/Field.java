package barthpt.games.grid;

import barthpt.games.mechanic.DataMap;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.LinkedHashMap;

public class Field extends DataMap {
    private final LinkedHashMap<Integer, Cell> cells;
    private final Rectangle endGame;

    public Field(int col, int gridSize) {
        this.cells = new LinkedHashMap<>();

        int padding = 10;
        dataInt.put("padding", padding);
        endGame = new Rectangle(padding,padding,gridSize,gridSize);

        dataInt.put("gridSize", gridSize);
        dataInt.put("col", col);

        double cellSize = (double) gridSize / col;
        dataDouble.put("H", cellSize);
        dataDouble.put("W", cellSize);
    }

    public void makeField(){
        for (int i = 0; i < dataInt.get("col"); i++) {
            for (int j = 0; j < dataInt.get("col"); j++) {
                cells.put( i*100 + j , new Cell( i, j, dataDouble.get("W"), dataDouble.get("H")));
            }
        }
    }

    public void init() {
        endGame.setColor(Color.MAGENTA);
        for (Integer key: cells.keySet()){
            cells.get(key).init();
        }
        endGame.draw();
    }

    public int randomCellID(){
        int col = (int) (Math.random() * dataInt.get("col")) * 100;
        int row = (int) (Math.random() * dataInt.get("col"));

        return  col + row;
    }
    public Cell getCell(int id){
        return cells.get(id);
    }

    @Override
    public void saveData() {

    }

    public boolean contains(int headUpdate) {
        return cells.containsKey(headUpdate);
    }

    public int size() {
        return cells.size();
    }
}
