package barthpt.games.mechanic;

import java.util.LinkedHashMap;

public abstract class DataMap {
    protected final LinkedHashMap<String,Integer> dataInt = new LinkedHashMap<>();
    protected final LinkedHashMap<String,Double> dataDouble = new LinkedHashMap<>();
    protected final LinkedHashMap<String,Boolean> dataBoolean = new LinkedHashMap<>();

    public abstract void saveData();

}
