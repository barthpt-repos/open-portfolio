package barthpt.games.mechanic;

import barthpt.games.grid.Field;

public class Game extends DataMap {
    private final Field field;
    private final Snake snake;
    //private final Rectangle endGame;
    public Game( int col, int gridSize) {
        field = new Field(col, gridSize);
        field.makeField();
        snake = new Snake(field);
        makeStats();
        init();
    }

    private void makeStats() {
        dataInt.put("GameDelay", 400);
        dataInt.put("fed#",0);
        dataInt.put("foodDelay",10);
        dataInt.put("cycle", 0);
        dataInt.put("score",0);

        dataBoolean.put("foodInGame", false);
    }
    private void init(){
        field.init();
        snake.init();

    }
    private void fedCounter() {
        dataInt.replace("fed#", dataInt.get("fed#") + 1);
        dataInt.replace("foodDelay", dataInt.get("cycle") + 10);

        dataBoolean.replace("foodInGame", false);
    }
    private boolean isGameOutOfFood() {
        return dataInt.get("foodDelay") > dataInt.get("cycle") &&
                !dataBoolean.get("foodInGame");
    }

    private void counter() {
        dataInt.replace("cycle", dataInt.get("cycle") + 1);
    }

    private long delayBalance() {
        return (dataInt.get("GameDelay") - dataInt.get("fed#")/2);
    }

    private void makeFood() {
        int foodCell = field.randomCellID();
        while (snake.contains(foodCell)) {
            foodCell = field.randomCellID();
        }
        dataBoolean.replace("foodInGame", true);
        field.getCell(foodCell).foodOn();
    }

    public void play(){
        while (true) {
            try {
                Thread.sleep(delayBalance());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (snake.safeToMove()){
                snake.move();
            } else {
                System.out.println("Game Over");
                break;
            }
            // Create food if there is no food on grid and 10 cycles after snakes has been fed
            if (isGameOutOfFood()) makeFood();

            // Verify is snake eaten food to restart cycle of makeFood
            if (snake.isFed()) fedCounter();


            if (isSnakeFullField()) {
                System.out.println("You win");
                break;
            }
            counter();
        }
        // Create Game Score
        //Info.replace("score", Info.get("snakeFed") * 20 - Info.get("counter"));
        //System.out.println("With Score of" + Info.get("score"));
        System.exit(0);
    }

    private boolean isSnakeFullField() {
        return snake.size() == field.size();
    }

    @Override
    public void saveData() {

    }
}
