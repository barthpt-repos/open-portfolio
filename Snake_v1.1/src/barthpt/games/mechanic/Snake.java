package barthpt.games.mechanic;

import barthpt.games.grid.Direction;
import barthpt.games.grid.Field;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.util.LinkedList;

public class Snake extends DataMap implements KeyboardHandler {
    private final Field field;
    private final LinkedList<Integer> cellList;
    private int headUpdate;
    private boolean fed;
    private Direction direction;

    public Snake(Field field){
        this.field = field;
        cellList = new LinkedList<>();
    }

    public void init() {
        keyboardInit();
        firstHead();
        updateData();
    }

    private void firstHead() {
        cellList.addFirst( (int)(Math.random() * field.dataInt.get("col")) );
        field.getCell( cellList.getFirst() ).snakeIn();
        direction = Direction.RIGHT;
    }

    public boolean contains(int foodCell) {
        return cellList.contains(foodCell);
    }

    public boolean safeToMove() {
        headUpdate = direction.getDir() + cellList.getFirst();
        if (!field.contains(headUpdate)){
            return false;
        }
        return !cellList.contains(headUpdate);
    }

    public void move() {
        headMove();
        if ((fed = field.getCell(head()).isFood())){
            headEat();
            updateData();
        } else {
            tailMove();
        }
    }

    private void headMove() {
        cellList.addFirst(headUpdate);
        field.getCell(headUpdate).snakeIn();
    }
    private void headEat() {
        field.getCell(headUpdate).eatFood();
    }
    private int head(){
        return cellList.getFirst();
    }


    private void tailMove() {
        field.getCell(cellList.getLast()).snakeOut();
        cellList.removeLast();
    }
    private void updateData() {
        dataInt.put("snakeSize",cellList.size());
    }


    public boolean isFed() {
        return fed;
    }

    private void keyboardInit() {
        Keyboard keyboard = new Keyboard(this);

        //RIGHT KEY to moveOK right
        KeyboardEvent rightKeyPressed = new KeyboardEvent();
        rightKeyPressed.setKey(KeyboardEvent.KEY_RIGHT);
        rightKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightKeyPressed);

        //LEFT KEY to moveOK left
        KeyboardEvent leftKeyPressed = new KeyboardEvent();
        leftKeyPressed.setKey(KeyboardEvent.KEY_LEFT);
        leftKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftKeyPressed);

        //UP KEY to moveOK up
        KeyboardEvent upKeyPressed = new KeyboardEvent();
        upKeyPressed.setKey(KeyboardEvent.KEY_UP);
        upKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(upKeyPressed);

        //DOWN KEY to moveOK down
        KeyboardEvent downKeyPressed = new KeyboardEvent();
        downKeyPressed.setKey(KeyboardEvent.KEY_DOWN);
        downKeyPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(downKeyPressed);
    }

    public void keyPressed(KeyboardEvent keyboardEvent) {
        int key = keyboardEvent.getKey();

        if (key == KeyboardEvent.KEY_LEFT) {
            direction = Direction.LEFT;
        }
        if (key == KeyboardEvent.KEY_RIGHT){
            direction = Direction.RIGHT;
        }
        if (key == KeyboardEvent.KEY_DOWN){
            direction = Direction.DOWN;
        }
        if (key == KeyboardEvent.KEY_UP){
            direction = Direction.UP;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
    public int size() {
        return cellList.size();
    }

    @Override
    public void saveData() {

    }
}
